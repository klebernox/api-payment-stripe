package dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PaymentDTO implements Serializable {

	private static final long serialVersionUID = 5993890598203829443L;

	private String token;
	private Double amount;
	
}
