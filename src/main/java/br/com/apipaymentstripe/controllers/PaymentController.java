package br.com.apipaymentstripe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stripe.model.Charge;

import br.com.apipaymentstripe.services.StripeService;
import dto.PaymentDTO;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/payment")
public class PaymentController {

	@Autowired
	private StripeService stripeService;

	@ApiOperation(value="Realiza pagamento atraves API Stripe.")
    @PostMapping("/charge")
    public Charge chargeCard(@RequestBody PaymentDTO payment) throws Exception {
        return this.stripeService.chargeCreditCard(payment.getToken(), payment.getAmount());
    }
    
}
