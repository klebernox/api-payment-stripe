package br.com.apipaymentstripe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPaymentStripeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPaymentStripeApplication.class, args);
		
	}

}
