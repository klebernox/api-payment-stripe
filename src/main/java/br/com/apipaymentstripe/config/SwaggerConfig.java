package br.com.apipaymentstripe.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Header;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Value("${spring.application.name}")
	private String appName;
	@Value("${spring.application.version}")
	private String version;
	
	private final ResponseMessage m201 = customMessage1();
	private final ResponseMessage m422 = simpleMessage(422, "Erro de validação");
	private final ResponseMessage m500 = simpleMessage(500, "Erro inesperado");

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.POST, Arrays.asList(m201, m422, m500))
				.select()
				.apis(RequestHandlerSelectors.basePackage("br.com.apipaymentstripe..resources"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(this.apiInfo());
	}
	
	@SuppressWarnings("rawtypes")
	private ApiInfo apiInfo() {
		return new ApiInfo(
				appName, 
				"API para pagamento via Stripe.", 
				version, 
				"http://www.ksgprod.com.br", 
				new Contact("Kleber Santos", "http://www.ksgprod.com.br", "klebernox@gmail.com"), 
				"Permitido para uso didático.", 
				"http://www.ksgprod.com.br", 
				new ArrayList<VendorExtension>());
	}
	
	private ResponseMessage simpleMessage(int code, String msg) {
		return new ResponseMessageBuilder().code(code).message(msg).build();
	}
	
	private ResponseMessage customMessage1() {
		Map<String, Header> map = new HashMap<String, Header>();
		map.put("location", 
				new Header("location", "URI do novo recurso", new ModelRef("string")));
		return new ResponseMessageBuilder()
				.code(201)
				.message("Recurso criado")
				.headersWithDescription(map)
				.build();
	}
}
