package br.com.apipaymentstripe.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stripe.Stripe;
import com.stripe.model.Charge;

@Service
public class StripeService {

	private static final String CURRENCY_TYPE = "BRL"; // https://stripe.com/docs/currencies
//	private static final String API_KEY = "sk_test_XXXXXXXXXXXXXXXXX";
	private static final String API_KEY = "sk_test_4IPEXGZsifmqHcEhHkGyhf4v00Ldt5CZ8f";
	
	@Autowired
	StripeService() {
		Stripe.apiKey = API_KEY;
	}

	/*
	 * o metodo retorna um objeto Charge da api do Stripe, contendo informacoes da
	 * transacao. eh aconselhavel associar informacoes trazidas ao usuario
	 * responsavel pela compra, afim de manter um historico de compras do usuario.
	 * 
	 * para mais informacoes, visite a documentacao do stripe:
	 * https://stripe.com/docs/api
	 */
	public Charge chargeCreditCard(String token, double amount) throws Exception {
		Map<String, Object> chargeParams = new HashMap<String, Object>();
		chargeParams.put("amount", (int) (amount * 100));
		chargeParams.put("currency", CURRENCY_TYPE);
		chargeParams.put("source", token);
		return Charge.create(chargeParams);
	}

}
