# Visão geral

O projeto é uma aplicação back-end com objetivo de demonstrar uma arquitetura básica para pagamento on-line, utilizando a API [Stripe](https://stripe.com/en-br).

## Tecnologias

- [Spring Boot](https://projects.spring.io/spring-boot) é uma ferramenta que simplifica a configuração e execução de aplicações Java stand-alone,  com conceitos de dependências “starters”, auto configuração e servlet container embutidos é proporcionado uma grande produtividade desde o start-up da aplicação até sua ida a produção.

 
# Setup da aplicação (local)

## Pré-requisito

Antes de rodar a aplicação é preciso garantir que as seguintes dependências estejam corretamente instaladas:
```
Java 8
Maven 3.3.3 
```

## Preparando ambiente

É necessário a criação de uma conta no site do [Stripe](https://dashboard.stripe.com/login). 

Depois de fazer isso, você deve obter as chaves da API. Vá para a guia "Get your test API keys" no menu e você encontrará as chaves Pública e Secreta.

Abra o arquivo:
```
application.properties
```

Copie sua chave secreta "Secret key" na propriedade do arquivo:
```
stripe.api.key
```


## Instalação da aplicação

Primeiramente, faça o clone do repositório:
```
git clone https://klebernox@bitbucket.org/klebernox/api-payment-stripe.git
```
Feito isso, acesse o projeto:
```
cd api-payment-stripe
```
É preciso compilar o código e baixar as dependências do projeto:
```
mvn clean dependency:resolve eclipse:eclipse
```
Finalizado esse passo, vamos iniciar a aplicação:
```
mvn spring-boot:run
```
Pronto. A aplicação está disponível em http://localhost:8080
```
Tomcat started on port(s): 8080 (http)
Started AppConfig in xxxx seconds (JVM running for xxxx)
```

## Swagger

A API fornece uma breve documentação via Swagger. Após startada, você poderá acessá-la através da url:
```
http://localhost:8080/swagger-ui.html
```

# APIs

O projeto disponibiliza a API de pagamento com o Stripe: Payment, onde utilizam o padrão Rest de comunicação, produzindo e consumindo arquivos no formato JSON.

Segue abaixo o endpoint da API disponível:

#### Payment

 - /payment/charge (POST)
     - Espera atributos para serem critérios de persistência na requisição, exemplo:
    ```
    {
      "token":"xxxxxxxxxxxxxxxxxx",
      "amount": 100
    }
    ```

